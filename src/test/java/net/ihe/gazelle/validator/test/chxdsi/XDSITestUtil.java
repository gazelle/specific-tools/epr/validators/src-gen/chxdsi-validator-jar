package net.ihe.gazelle.validator.test.chxdsi;

import net.ihe.gazelle.chxdsi.validator.chxdsi.CHXDSIPackValidator;
import net.ihe.gazelle.validation.Notification;
import net.ihe.gazelle.validator.test.common.AbstractValidator;
import net.ihe.gazelle.xds.ProvideAndRegisterDocumentSetRequestType;

import java.util.List;

public class XDSITestUtil extends AbstractValidator<ProvideAndRegisterDocumentSetRequestType> {

    @Override
    protected void validate(ProvideAndRegisterDocumentSetRequestType message,
                            List<Notification> notifications) {
        ProvideAndRegisterDocumentSetRequestType.validateByModule(message, "/AssertionType", new CHXDSIPackValidator(), notifications);
    }

    @Override
    protected Class<ProvideAndRegisterDocumentSetRequestType> getMessageClass() {
        return ProvideAndRegisterDocumentSetRequestType.class;
    }
}