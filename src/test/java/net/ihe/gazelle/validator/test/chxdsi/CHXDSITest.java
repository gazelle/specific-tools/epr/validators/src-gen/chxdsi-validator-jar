package net.ihe.gazelle.validator.test.chxdsi;

import org.junit.Assert;
import org.junit.Test;

public class CHXDSITest {

    XDSITestUtil testExecutor = new XDSITestUtil();

    @Test
    public void test_ko_constraint_studyInstanceUID() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDSI_studyInstance_KO.xml",
                        "chxdsi-CHXDSI_ExtrinsicObjectSpec-CHstudyInstanceUIDConstraint"));
    }

    @Test
    public void test_ok_constraint_studyInstanceUID() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDSI_studyInstance_OK.xml",
                        "chxdsi-CHXDSI_ExtrinsicObjectSpec-CHstudyInstanceUIDConstraint"));
    }

    @Test
    public void test_ok_constraint_mimeTypeDICOM() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDSI_OK.xml",
                        "chxdsi-CHXDSI_ExtrinsicObjectSpec-CHMimeTypeIsDICOMConstraint"));
    }

    @Test
    public void test_ko_constraint_mimeTypeDICOM() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDSI_mimeType_KO.xml",
                        "chxdsi-CHXDSI_ExtrinsicObjectSpec-CHMimeTypeIsDICOMConstraint"));
    }

    @Test
    public void test_ok_constraint_formatCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDSI_OK.xml",
                        "chxdsi-CHXDSI_ExtrinsicObjectSpec-CHFormatCodeIsDICOMManifestConstraint"));
    }

    @Test
    public void test_ok_constraint_formatCodeName() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnValidFile(
                        "src/test/resources/CHXDSI_formatCodeVSName_OK.xml",
                        "chxdsi-CHXDSI_ExtrinsicObjectSpec-CHFormatCodeIsDICOMManifestConstraint"));
    }

    @Test
    public void test_ko_constraint_formatCodeVSCodeSystem() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDSI_formatCodeVSCodeSystem_KO.xml",
                        "chxdsi-CHXDSI_ExtrinsicObjectSpec-CHFormatCodeIsDICOMManifestConstraint"));
    }

    @Test
    public void test_ko_constraint_formatCodeVSCode() {
        Assert.assertTrue(testExecutor
                .checkConstraintOnNonValidFile(
                        "src/test/resources/CHXDSI_formatCodeVSCode_KO.xml",
                        "chxdsi-CHXDSI_ExtrinsicObjectSpec-CHFormatCodeIsDICOMManifestConstraint"));
    }
}
