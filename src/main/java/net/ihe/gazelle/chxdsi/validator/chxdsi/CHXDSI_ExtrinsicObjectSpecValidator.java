package net.ihe.gazelle.chxdsi.validator.chxdsi;

import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.LocalizedStringType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import java.util.List;



 /**
  * class :        CHXDSI_ExtrinsicObjectSpec
  * package :   chxdsi
  * Constraint Spec Class
  * class of test : ExtrinsicObjectType
  * 
  */
public final class CHXDSI_ExtrinsicObjectSpecValidator{


    private CHXDSI_ExtrinsicObjectSpecValidator() {}



	/**
	* Validation of instance by a constraint : CHFormatCodeIsDICOMManifestConstraint
	* The DocumentEntry.formatCode SHALL be equal to the coded value of “DICOM Manifest” (“1.2.840.10008.5.1.4.1.1.88.59^^1.2.840.10008.2.6.1”).
	*
	*/
	private static boolean _validateCHXDSI_ExtrinsicObjectSpec_CHFormatCodeIsDICOMManifestConstraint(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
		java.util.ArrayList<ClassificationType> result1;
		result1 = new java.util.ArrayList<ClassificationType>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (ClassificationType anElement1 : aClass.getClassification()) {
			    Boolean result2;
			    result2 = true;
			    
			    /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
			    try{
			    	for (SlotType1 anElement2 : anElement1.getSlot()) {
			    	    Boolean result3;
			    	    result3 = true;
			    	    
			    	    /* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
			    	    try{
			    	    	for (String anElement3 : anElement2.getValueList().getValue()) {
			    	    	    if (!anElement3.equals("1.2.840.10008.2.6.1")) {
			    	    	        result3 = false;
			    	    	        break;
			    	    	    }
			    	    	    // no else
			    	    	}
			    	    }
			    	    catch(Exception e){}
			    
			    	    if (!result3) {
			    	        result2 = false;
			    	        break;
			    	    }
			    	    // no else
			    	}
			    }
			    catch(Exception e){}

				Boolean result4;
				result4 = true;

				/* Iterator ForAll: Iterate and check, if all elements fulfill the condition. */
				try{
					for (LocalizedStringType anElement4 : anElement1.getName().getLocalizedString()) {
						if (!!(anElement4 == null)) {
							result4 = false;
							break;
						}
						// no else
					}
				}
				catch(Exception e){}
		
			    if (((((!(((String) anElement1.getClassificationScheme()) == null) && anElement1.getClassificationScheme().equals("urn:uuid:a09d5840-386c-46f2-b5ad-9c3699a4309d")) && anElement1.getNodeRepresentation().equals("1.2.840.10008.5.1.4.1.1.88.59")) && result2) && result4)) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > new Integer(0));
		
		
	}

	/**
	* Validation of instance by a constraint : CHMimeTypeIsDICOMConstraint
	* DocumentEntry.mimeType SHALL be equal to “application/dicom”
	*
	*/
	private static boolean _validateCHXDSI_ExtrinsicObjectSpec_CHMimeTypeIsDICOMConstraint(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
		return ((!(((String) aClass.getMimeType()) == null) && !aClass.getMimeType().equals("")) && aClass.getMimeType().equals("application/dicom"));
		
	}

	/**
	* Validation of instance by a constraint : CHstudyInstanceUIDConstraint
	* When a Imaging Document Source provides a document to the Document Repository, it must provide the StudyInstanceUID, found in the to be registered KOS object, in the referenceIdList (urn:ihe:iti:xds:2013:referenceIdList) attribute of the documentEntry metadata.
	*
	*/
	private static boolean _validateCHXDSI_ExtrinsicObjectSpec_CHstudyInstanceUIDConstraint(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
		java.util.ArrayList<SlotType1> result1;
		result1 = new java.util.ArrayList<SlotType1>();
		
		/* Iterator Select: Select all elements which fulfill the condition. */
		try{
			for (SlotType1 anElement1 : aClass.getSlot()) {
			    if (((anElement1.getName().equals("urn:ihe:iti:xds:2013:referenceIdList") && !(anElement1.getValueList() == null)) && (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(anElement1.getValueList().getValue()) > new Integer(0)))) {
			        result1.add(anElement1);
			    }
			    // no else
			}
		}
		catch(Exception e){}
		
		return (tudresden.ocl20.pivot.tools.codegen.ocl2java.types.util.OclCollections.size(result1) > new Integer(0));
		
		
	}

	/**
	* Validation of class-constraint : CHXDSI_ExtrinsicObjectSpec
    * Verify if an element of type CHXDSI_ExtrinsicObjectSpec can be validated by CHXDSI_ExtrinsicObjectSpec
	*
	*/
	public static boolean _isCHXDSI_ExtrinsicObjectSpec(net.ihe.gazelle.rim.ExtrinsicObjectType aClass){
			return true;	
	}

/**
     * Validate as a template
     * name ::   CHXDSI_ExtrinsicObjectSpec
     * class ::  net.ihe.gazelle.rim.ExtrinsicObjectType
     * 
     */
    public static void _validateCHXDSI_ExtrinsicObjectSpec(net.ihe.gazelle.rim.ExtrinsicObjectType aClass, String location, List<Notification> diagnostic) {
		if (_isCHXDSI_ExtrinsicObjectSpec(aClass)){
			executeCons_CHXDSI_ExtrinsicObjectSpec_CHFormatCodeIsDICOMManifestConstraint(aClass, location, diagnostic);
			executeCons_CHXDSI_ExtrinsicObjectSpec_CHMimeTypeIsDICOMConstraint(aClass, location, diagnostic);
			executeCons_CHXDSI_ExtrinsicObjectSpec_CHstudyInstanceUIDConstraint(aClass, location, diagnostic);
		}
	}

	private static void executeCons_CHXDSI_ExtrinsicObjectSpec_CHFormatCodeIsDICOMManifestConstraint(net.ihe.gazelle.rim.ExtrinsicObjectType aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXDSI_ExtrinsicObjectSpec_CHFormatCodeIsDICOMManifestConstraint(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("CHFormatCodeIsDICOMManifestConstraint");
		notif.setDescription("The DocumentEntry.formatCode SHALL be equal to the coded value of “DICOM Manifest” (“1.2.840.10008.5.1.4.1.1.88.59^^1.2.840.10008.2.6.1”).");
		notif.setLocation(location);
		notif.setIdentifiant("chxdsi-CHXDSI_ExtrinsicObjectSpec-CHFormatCodeIsDICOMManifestConstraint");
		
		diagnostic.add(notif);
	}

	private static void executeCons_CHXDSI_ExtrinsicObjectSpec_CHMimeTypeIsDICOMConstraint(net.ihe.gazelle.rim.ExtrinsicObjectType aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXDSI_ExtrinsicObjectSpec_CHMimeTypeIsDICOMConstraint(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("CHMimeTypeIsDICOMConstraint");
		notif.setDescription("DocumentEntry.mimeType SHALL be equal to “application/dicom”");
		notif.setLocation(location);
		notif.setIdentifiant("chxdsi-CHXDSI_ExtrinsicObjectSpec-CHMimeTypeIsDICOMConstraint");
		
		diagnostic.add(notif);
	}

	private static void executeCons_CHXDSI_ExtrinsicObjectSpec_CHstudyInstanceUIDConstraint(net.ihe.gazelle.rim.ExtrinsicObjectType aClass, 
			String location, List<Notification> diagnostic) {
		Notification notif = null;
		try {
			if (!(_validateCHXDSI_ExtrinsicObjectSpec_CHstudyInstanceUIDConstraint(aClass) )){
				notif = new Error();
			}
			else {
				notif = new Note();
			}
		}
		catch(Exception e) {
			notif = new Error();
		}
		notif.setTest("CHstudyInstanceUIDConstraint");
		notif.setDescription("When a Imaging Document Source provides a document to the Document Repository, it must provide the StudyInstanceUID, found in the to be registered KOS object, in the referenceIdList (urn:ihe:iti:xds:2013:referenceIdList) attribute of the documentEntry metadata.");
		notif.setLocation(location);
		notif.setIdentifiant("chxdsi-CHXDSI_ExtrinsicObjectSpec-CHstudyInstanceUIDConstraint");
		
		diagnostic.add(notif);
	}

}
