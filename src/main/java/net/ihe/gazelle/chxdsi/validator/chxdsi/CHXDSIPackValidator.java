package net.ihe.gazelle.chxdsi.validator.chxdsi;

import java.util.ArrayList;
import java.util.List;

import net.ihe.gazelle.validation.Error;
import net.ihe.gazelle.validation.Warning;
import net.ihe.gazelle.validation.Info;
import net.ihe.gazelle.validation.Note;
import net.ihe.gazelle.validation.Notification;

import net.ihe.gazelle.gen.common.ConstraintValidatorModule;


import net.ihe.gazelle.rim.ClassificationType;
import net.ihe.gazelle.rim.InternationalStringType;
import net.ihe.gazelle.rim.LocalizedStringType;
import net.ihe.gazelle.rim.SlotType1;
import net.ihe.gazelle.rim.ValueListType;



public class CHXDSIPackValidator implements ConstraintValidatorModule{


    /**
     * Create a new ObjectValidator that can be used to create new instances of schema derived classes for package: generated
     * 
     */
    public CHXDSIPackValidator() {}
    


	/**
	* Validation of instance of an object
	*
	*/
	public void validate(Object obj, String location, List<Notification> diagnostic){

		if (obj instanceof net.ihe.gazelle.rim.ExtrinsicObjectType){
			net.ihe.gazelle.rim.ExtrinsicObjectType aClass = ( net.ihe.gazelle.rim.ExtrinsicObjectType)obj;
			CHXDSI_ExtrinsicObjectSpecValidator._validateCHXDSI_ExtrinsicObjectSpec(aClass, location, diagnostic);
		}
	
	}

}

